import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';

@Injectable()
export class EmployeeService{
    
    private listEmployees:Employee[]=[
        {
            id:1,
            fName:'Mark',
            gender:'Male',
            contactPreference:'EmailPreference',
            email:'mark@pragimtech.com',
            dateOfBirth:new Date('10/25/1988'),
            department:'1',
            phoneNumber: 1234567890,
            isActive:true,
            photoPath:'assets/Images/employee-Girl.png'
            },
            {
              id:2,
              fName:'Mary',
              gender:'FeMale',
              contactPreference:'EmailPreference',
              email:'mary@pragimtech.com',
              dateOfBirth:new Date('10/25/1979'),
              department:'1',
              phoneNumber: 1234567899,
              isActive:true,
              photoPath:'assets/Images/employee-men.png'
            },
    ];

    getEmployees():Employee[] {
        return this.listEmployees;
    }
        save(employee:Employee){
            this.listEmployees.push(employee);
        }
    

}